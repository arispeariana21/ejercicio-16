import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  var1 = 'Harry';
  var2 = 'Niall';
  var3 = 'Zayn';
  var4 = 'Liam';
  var5 = 'Louis'
  
  constructor() { }

  ngOnInit(): void {
  }

}
