import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  @Input() msg1!: string;
  @Input() msg2!: string;
  @Input() msg3!: string;
  @Input() msg4!: string;
  @Input() msg5!: string;

  constructor() { }

  ngOnInit(): void {
  }

}
